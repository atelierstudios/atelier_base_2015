jQuery.noConflict();

// Put all your code in your document ready area

jQuery(document).ready(function($){

// Do jQuery stuff using $ this where the functions need to go   


		$(document).ready(function() {
			
// =============== COOKIE CUTTR Function - jquery.cookiecuttr.js  =============== 
			
		$.cookieCuttr( 
			{
			cookieAnalytics: true,	
			cookieAnalyticsMessage: 'We use cookies on our website. To use the website as intended please...',
			cookieWhatAreTheyLink: '/privacy-policy/',
			cookieNotificationLocationBottom: true
			}
		);
		
		
// ======================= ADD PRINT BUTTON TO BOTTOM UL =========================
		
			$('a.print').click(function() {
				window.print();
				return false;
			});
			
// ======================= DROP DOWN NAV - NO PLUGIN - Taken from kickstart.js - http://www.99lime.com/ =========================

		
			$('ul.main_nav li').hover(function(){
				$(this).find('ul:first').stop(true, true).fadeIn('fast');
				$(this).addClass('hover');
			},
			function(){
				$(this).find('ul').stop(true, true).fadeOut('slow');
				$(this).removeClass('hover');
			});		
	

		
		});

});