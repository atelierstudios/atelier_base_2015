<?php
/**
 * @package WordPress
 * @subpackage atelier
 */

get_header(); ?>

	<div id="content" class="nine columns float_right">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
            <h1><?php the_title(); ?></h1>

            <?php the_post_thumbnail('Featured Image', array('class' => 'featured_image')); ?>

			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            
		<?php endwhile; endif; ?>
		
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>