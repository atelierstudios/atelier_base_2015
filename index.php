<?php
/**
 * @package WordPress
 * @subpackage atelier
 */

get_header(); ?>

	<div id="content" class="nine columns float_right">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>

		<?php endwhile; ?>

	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>