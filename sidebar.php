<?php
/**
 * @package WordPress
 * @subpackage atelier
 */
?>

	<aside id="sidebar" class="three columns no_margin float_left no_print">
 
 
	  <?php function get_root_parent($page_id) {
                global $wpdb;
                $parent = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE post_type='page' AND post_status='publish' AND ID = '$page_id'");
                if ($parent == 0) return $page_id;
                else return get_root_parent($parent);
            }
            global $post;
            $page_id = $post->ID;
            //Here is the Root Parent page
            $root_parent=get_root_parent($page_id);
        ?> 
        <?php
            $post_parent_details = get_post($root_parent);
            //Here is the page title
            $post_parent_title = $post_parent_details->post_title;
            //Here is the page link
            $post_parent_link = get_permalink($root_parent)
        ?>
        
        <?php
            $g_page_id = $wp_query->get_queried_object_id();
            $ancestorIDs = _wswwpx_page_get_ancestor_ids($g_page_id);		
            $children = wswwpx_fold_page_list('title_li=&sort_column=menu_order&child_of='.$ancestorIDs[1].'&echo=0');
            if ($children) { 
        ?>
            <ul class="sidebar_nav margin_bottom_30">
                <?php echo $children; ?>
            </ul>
        <?php } ?>  
 
    
		<?php
        	/* When we call the dynamic_sidebar() function, it'll spit out
            * the widgets for that widget area.
            */
                if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
            
        <?php endif; // end primary widget area ?>    
 
 	
        <div class="row hide-on-phones">
            <div class="tweleve columns">
            	<div class="panel">
            
                <h5>Contact us Today</h5>
            
                <p>Contact us today on <strong>02380 768652</strong> or send an enquiry using the button below</p>
                
                <a href="/contact-us/" class="medium radius black button">Contact Us</a>
        		</div>
            </div> 
        </div> 


	</aside>
