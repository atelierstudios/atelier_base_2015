<?php
/**
 * TwentyTen functions and definitions
 *
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
	if ( ! isset( $content_width ) )
		$content_width = 640;


/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
	add_action( 'after_setup_theme', 'twentyten_setup' );

	if ( ! function_exists( 'twentyten_setup' ) ):


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override twentyten_setup() in a child theme, add your own twentyten_setup to your child theme's
 * functions.php file.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style( 'child-editor-style.css' );

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'twentyten', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'twentyten' ),
	) );

}
endif;



/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * To override this in a child theme, remove the filter and optionally add
 * your own function tied to the wp_page_menu_args filter hook.
 *
 * @since Twenty Ten 1.0
 */
	function twentyten_page_menu_args( $args ) {
		$args['show_home'] = true;
		return $args;
	}
	add_filter( 'wp_page_menu_args', 'twentyten_page_menu_args' );

/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Twenty Ten 1.0
 * @return int
 */

	function bm_better_excerpt($length, $ellipsis) {
	$text = get_the_content();
	$text = strip_tags($text);
	$text = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $text );
	$text = substr($text, 0, $length);
	$text = substr($text, 0, strripos($text, " "));
	$text = $text.$ellipsis;
	return $text;
	}
	add_filter('the_excerpt', 'gpp_excerpt');


/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @since Twenty Ten 1.0
 * @return string "Continue Reading" link
 */
	function twentyten_continue_reading_link() {
		return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyten' ) . '</a>';
	}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyten_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string An ellipsis
 */
	function twentyten_auto_excerpt_more( $more ) {
		return ' &hellip;' . twentyten_continue_reading_link();
	}
	add_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string Excerpt with a pretty "Continue Reading" link
 */
	function twentyten_custom_excerpt_more( $output ) {
		if ( has_excerpt() && ! is_attachment() ) {
			$output .= twentyten_continue_reading_link();
		}
		return $output;
	}
	add_filter( 'get_the_excerpt', 'twentyten_custom_excerpt_more' );



/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */

function twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'twentyten' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

}


/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
	add_action( 'widgets_init', 'twentyten_widgets_init' );


/**
 *----------------- Register New Custom Post Type for Homepage Banners -------------------
**/

add_action('init', 'homepage_banner_images_register');

function homepage_banner_images_register () {

    $args = array(
		'label' => __('Home Page Banners'),
		'singular_label' => __('Home Page Banner'),
		'public' => true,
		'show_ui' => true, // UI in admin panel
		'_builtin' => false, // It's a custom post type, not built in!
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => false,
		'has_archive' => true,
		'rewrite' => array("slug" => "home-page-banners"), // Permalinks format
		'supports' => array('title', 'editor', 'thumbnail', 'custom-fields', 'excerpt', 'page-attributes' ),
		'menu_icon' => get_stylesheet_directory_uri() . '/furniture/images/admin_area/homepagebanners.png',
    );

    register_post_type( 'home-page-banners' , $args );
}


/** -------------- REGISTER CUSTOM TAXONOMIES ----------------- **/

/**
 *----------------- Function for conditional statement is_tree( 'id' ) to see if the current page is the page, or is a sub page of the page.
 */


function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;               // load details about this page

    if ( is_page($pid) )
        return true;            // we're at the page or at a sub page

    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return true;
        }
    }

    return false;  // we arn't at the page, and the page is not an ancestor
}


//--------- Remove Tool Tip from wp_list_page nav items ------------

function remove_title($input) {
  return preg_replace_callback('#\stitle=["|\'].*["|\']#',
    create_function(
      '$matches',
      'return "";'
      ),
      $input
    );
  }
add_filter('wp_list_pages','remove_title');


/**
 *----------------- Stop Contact Form 7 adding Javascript and CSS to page -------------------
**/

add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );
function my_deregister_javascript() {
  if ( !is_page('29') ) {
    wp_deregister_script( 'contact-form-7' );
  }
}

add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles() {
wp_deregister_style( 'contact-form-7' );
}


/**
 *----------------- ADD FEATURED IMAGE SIZES -------------------
**/

add_image_size( 'Home Banner', 650, 230, true );
add_image_size( 'Parent Page Banner', 980, 230, true );
add_image_size( 'Featured Image', 300, 300, true );
add_image_size( 'Barristers Main Profile Photo', 212, 212, true );


/**
 *----------------- Load latest version of jQuery from Google Libraries API and de-register Wordpress version -------------------
**/


if( !is_admin()){

wp_deregister_script('jquery');
wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"), false, '2.1.3');
wp_enqueue_script('jquery');
}


/****************************************************************/

	/*** ADMIN AREA TWEAKS - TAKEN FROM - http://www.thinkoomph.com/thinking/2010-06/customizing-wordpress-admin/ ***/

/****************************************************************/


// lots of front end functions? consider compartmentalizing admin:
// if ( is_admin() ) require_once('functions_admin.php');

if ( is_admin() ) : // why execute all the code below at all if we're not in admin?



/**************************/
/*** PART ONE: BRANDING ***/
/**************************/


/**
 * overriding footer "credit" text
 */

add_filter( 'admin_footer_text', 'custom_footer_text' );

function custom_footer_text($default_text)
{
	return '<span id="footer-thankyou">Site built by <a href="http://www.atelier-studios.com/" target="_blank">Atelier Studios</a><span> |
Powered by <a href="http://www.wordpress.org">WordPress</a> <br />
Need help? If you need additional support, you can submit a support ticket at <a href="http://helpdesk.atelierstudios.co.uk/">helpdesk.atelierstudios.co.uk</a>, Alternatively contact us on - Tel:</strong> +44 (0) 23 8022 7117';
}

// fewer lines of code - a cleaner version with lambda style function
// add_filter( 'admin_footer_text', create_function( '$a', 'return \'<span id="footer-thankyou">Site built by <a href="http://www.atelier-studios.com/" target="_blank">Atelier Studios</a><span> | Powered by <a href="http://www.wordpress.org">WordPress</a>\';' ) );


/**
 * cleaning up and customizing the dashboard
 */

add_action('wp_dashboard_setup', 'custom_dashboard_widgets');

function custom_dashboard_widgets() {
	global $wp_meta_boxes;

	// remove unnecessary widgets
	// var_dump( $wp_meta_boxes['dashboard'] ); // use to get all the widget IDs
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['acx_plugin_dashboard_widget']);


	wp_add_dashboard_widget('custom_help_widget', 'Help and Support', 'custom_dashboard_help'); // add a new custom widget for help and support
}


function custom_dashboard_help() {
	echo '
		<p>Need help? That "help" tab up top provides contextual help throughout the administrative panel. If you need additional support, you can submit a support ticket at <a href="http://helpdesk.atelierstudios.co.uk/">helpdesk.atelierstudios.co.uk</a> and we will try and resolve your problem as soon as we can.</p>
		<p>Alternatively contact us on:</p>
		<p><strong>Tel:</strong> +44 (0) 23 8022 7117</p>
	';
}


/**
 * custom contextual help - tack on our support information to the end of the contextual help
 */

add_filter( 'contextual_help', 'custom_help_support', 100 ); //giving a very low priority to make sure it's always at the end

function custom_help_support($help)
{
	$help .= '
		<p><strong>Additional support</strong> - submit a support ticket at <a href="http://helpdesk.atelierstudios.co.uk/">helpdesk.atelierstudios.co.uk</a> and we will try and resolve your problem as soon as we can. Alternatively contact us on - Tel:</strong> +44 (0) 23 8022 7117<p>
	';
	return $help;
}


/***********************************/
/*** PART TWO: CLEANING UP ADMIN ***/
/***********************************/

/**
 * custom "admin lite" role
 * we want an editor that can also: manage users, manage plugins, unfiltered upload, manage options
 */

// remove_role( 'adminlite' );

if ( !get_role('adminlite') )
{
	$caps = get_role('editor')->capabilities; //let's use the editor as the base  capabilities
	$caps = array_merge( $caps, array(
		'install_plugins' => true,
		'activate_plugins' => true,
		'update_plugins' => true,
		'delete_plugins' => true,
		'list_users' => true, //wp3.0
		'create_users' => true,
		'edit_users' => true,
		'delete_users' => true,
		'unfiltered_upload' => true,
		'edit_theme_options' => true //wp3.0
	)); //adding new capabilities: reference http://codex.wordpress.org/Roles_and_Capabilities#Capability_vs._Role_Table

	add_role( 'adminlite', 'Administrator Lite', $caps );
}

/**
 * Remove some of the Menus items in the left hand sidebar - http://www.sitepoint.com/how-to-hide-menus-in-wordpress/
 */

// remove unnecessary menus
function remove_admin_menus () {
	global $menu;
	// all users
	$restrict = explode(',', 'Links,Comments');
	// non-administrator users
	$restrict_user = explode(',', 'Profile,Appearance,Plugins,Users,Tools,Settings');
	// WP localization
	$f = create_function('$v,$i', 'return __($v);');
	array_walk($restrict, $f);
	if (!current_user_can('activate_plugins')) {
		array_walk($restrict_user, $f);
		$restrict = array_merge($restrict, $restrict_user);
	}
	// remove menus
	end($menu);
	while (prev($menu)) {
		$k = key($menu);
		$v = explode(' ', $menu[$k][0]);
		if(in_array(is_null($v[0]) ? '' : $v[0] , $restrict)) unset($menu[$k]);
	}
}
add_action('admin_menu', 'remove_admin_menus');


/**
 * let's eliminate some sidebar widgets we know the client will never use
 */

add_action( 'widgets_init', 'custom_remove_widgets' );

function custom_remove_widgets()
{
	unregister_widget( 'WP_Widget_Pages' );
	unregister_widget( 'WP_Widget_Categories' );
	unregister_widget( 'WP_Widget_Archives' );
	unregister_widget( 'WP_Widget_Meta' );
	unregister_widget( 'WP_Widget_Links' );
}


/**
 * meaningful post specific help
 */

add_filter( 'contextual_help', 'custom_post_help', 10, 2 );

function custom_post_help($help, $screen)
{
	global $post_type; //required in 3.0 to differentiate posts from pages and other content types

	if ( $screen = 'post' && $post_type == 'post' )
	{
		$help .= '
			<p><strong>Front Page Posts</strong> - Be sure to assign posts you want highlighted
			on the front page of the website to the "Featured" category.<p>
		';
	}

	return $help;
}


/**
 * Re order the nav / menu order in the admin area
 */

   function custom_menu_order($menu_ord) {
       if (!$menu_ord) return true;
       return array(
        'index.php', // Dashboard
        'edit.php?post_type=page', // Pages
        'edit.php', // Posts or News
        'edit.php?post_type=home-page-banners', // Homepage Banners
        'edit.php?post_type=case-studies' // Case Studies
    );
   }
   add_filter('custom_menu_order', 'custom_menu_order');
   add_filter('menu_order', 'custom_menu_order');


/**
 * Rename Posts to News - http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
 */

function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'News';
	$submenu['edit.php'][5][0] = 'News';
	$submenu['edit.php'][10][0] = 'Add News';
	$submenu['edit.php'][16][0] = 'News Tags';
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'News';
	$labels->singular_name = 'News';
	$labels->add_new = 'Add News';
	$labels->add_new_item = 'Add News';
	$labels->edit_item = 'Edit News';
	$labels->new_item = 'News';
	$labels->view_item = 'View News';
	$labels->search_items = 'Search News';
	$labels->not_found = 'No News found';
	$labels->not_found_in_trash = 'No News found in Trash';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );


// REMOVE THE WORDPRESS UPDATE NOTIFICATION FOR ALL USERS EXCEPT SYSADMIN

global $user_login;
    get_currentuserinfo();
    if (!current_user_can('update_plugins')) { // checks to see if current user can update plugins
    add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
    add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
 }


/**
 * adding the post ID to the post list... if there's time
 */

add_filter( 'manage_posts_columns', 'custom_post_id_column' );

function custom_post_id_column($post_columns) {
	$beginning = array_slice( $post_columns, 0, 1 );
	$beginning['postid'] = __('ID');
	$ending = array_slice( $post_columns, 1 );
	$post_columns = array_merge( $beginning, $ending );
	return $post_columns;
}

add_action( 'manage_posts_custom_column', create_function('$a,$b','if($a=="postid") echo $b;'), 10, 2 );

endif; //wrapper for admin functions
?>
