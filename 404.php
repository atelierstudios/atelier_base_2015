<?php
/**
 * @package WordPress
 * @subpackage atelier
 */
get_header(); ?>

	<div id="content" class="nine columns float_right">

				<h1><?php _e( 'Not Found', 'twentyten' ); ?></h1>
				<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'twentyten' ); ?></p>
				<?php get_search_form(); ?>

				<script type="text/javascript">
                    // focus on search field after it has loaded
                    document.getElementById('s') && document.getElementById('s').focus();
                </script>
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>