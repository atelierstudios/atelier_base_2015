<?php /* If there are no posts to display, such as an empty archive page */ ?>

<?php if ( ! have_posts() ) : ?>
		<h1><?php _e( 'Not Found', 'twentyten' ); ?></h1>
		<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyten' ); ?></p>
		<?php get_search_form(); ?>

<?php endif; ?>

<?php /* ----------- Start the Loop. --------------*/ ?>

<?php while ( have_posts() ) : the_post(); ?>



<?php /*--------------- RECENT WORK --------------------*/ ?>


<?php if ( 
	in_category( array( 3,4 ) )
) { ?>

                    <div class="news_article_container" <?php post_class() ?>>
                                                                        
                            <div class="recent_work_listing">
                                 <p class="small_text_11px"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
                                 <p class="small_text_11px"><? echo bm_better_excerpt(135, '... <a href="'.get_permalink().'">Read More &raquo;</a>'); ?></p>
                            </div>
            
                    </div> 


<?php /*--------------- NEWS  --------------------*/ ?>

 <?php } else if ( 
	'novo_news' == get_post_type()
) { ?>

                    <div class="news_article_container" <?php post_class() ?>>
                            
                            <p class="news_date small_text_10px"><strong><?php the_time('d M Y') ?></strong></p>
                                            
                            <div class="news_content">
                                 <p class="small_text_11px"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
                                 <p class="small_text_11px"><? echo bm_better_excerpt(135, '... <a href="'.get_permalink().'">Read More &raquo;</a>'); ?></p>
                            </div>
            
                    </div>  


<?php } ?>


<?php endwhile; // End the loop. Whew. ?>


<?php if (  $wp_query->max_num_pages > 1 ) : ?>

<div class="full_width_wrapper">
	<span class="align_left float_left"><?php next_posts_link( __( '&larr;', 'twentyten' ) ); ?></span>
	<span class="align_right float_right"><?php previous_posts_link( __( '&rarr;', 'twentyten' ) ); ?></span>
</div>
<?php endif; ?>
