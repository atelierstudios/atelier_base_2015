<?php
/**
 * @package WordPress
 * @subpackage atelier
 */

get_header(); ?>

	<div id="content" class="nine columns float_right">

<?php if ( have_posts() ) : ?>
				
                <h2><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h2>

   
   				<?php while ( have_posts() ) : the_post(); ?>

                    <article class="news_article_container row" <?php post_class() ?>>
                    	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    	<p><? echo bm_better_excerpt(400, '... <a href="'.get_permalink().'">Read More &raquo;</a>'); ?></p>    
                    </article>

				<?php endwhile; ?>
   
<?php else : ?>
					<h2><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
					<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
					<?php get_search_form(); ?>
<?php endif; ?>

	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
