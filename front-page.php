<?php
/**
 * @package WordPress
 * @subpackage atelier
 */

get_header(); ?>

<div class="sub_header no_print hide-on-phones">

    <div class="slide_container">
 
	<?php $loop = new WP_Query( array( 'post_type' => 'home-page-banners', 'posts_per_page' => 30, 'orderby' => 'menu_order', 'order' => 'ASC' ) ); ?>
            
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>   
 
        <div class="slide">
        	<div class="slide_wrapper slide_<?php the_ID(); ?>">
                <div class="slide_image"></div>
            	<div class="text_position">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>           
            </div>
        </div>
 
	<?php endwhile; ?>                   
     
    </div>

    <div class="slider_nav_conatiner">
       <ul class="slider_nav"></ul>
       <a href="#" class="next">Next</a>    
        <a href="#" class="previous">Previous</a>   
   	</div>
    
</div>

<div class="row">

	<div class="four columns hide-on-phones">
    	<div class="panel">
    	
        <h5>Latest News</h5>
        
            
    <ul>
    <?php
      $args = array( 'numberposts' => '5' );
      $recent_posts = wp_get_recent_posts( $args );
      foreach( $recent_posts as $recent ){
        echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
      }
    ?>
    </ul>
    	</div>
	</div>

	<div id="content" class="four columns">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
        <div class="post" id="post-<?php the_ID(); ?>">
			
            <h2><?php the_title(); ?></h2>
			
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            
		</div>
        
		<?php endwhile; endif; ?>
        
		<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
		
	</div>
    
	<div class="four columns">
    	<div class="panel">
    	<h5>Contact us Today</h5>
    
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, 
        semper suscipit, posuere a, pede.</p>
        
        <a href="/contact-us/" class="medium radius black button">Contact Us</a>
    	</div>
	</div>    
    
</div>

<?php get_footer(); ?>