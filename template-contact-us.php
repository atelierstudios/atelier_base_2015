<?php
/**
 * @package WordPress
 * @subpackage atelier
 * Template Name: Contact Us
 */

get_header(); ?>

 <div class="row">
	<div id="content" class="twelve columns">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
            <div class="row">
            
                <div class="five columns">
                
                	<div id="map"></div>
                    
                    <p class="small">View a larger version of this map <a href="http://maps.google.co.uk/maps/ms?hl=en&amp;ie=UTF8&amp;msa=0&amp;msid=116285637576366098321.00048922b2b928499fc68&amp;ll=51.518958,-0.112717&amp;spn=0,0&amp;source=embed" target="_blank">here</a>.</p>
                    
                </div>
               
                 <div class="three columns">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
                </div>
            
                <div class="four columns">
					<?php echo do_shortcode( '[contact-form-7 id="6" title="Contact form 1"]' ); ?>
                </div>
              
                
            </div>            
                    
		<?php endwhile; endif; ?>
		
	</div>
	</div>

<?php get_footer(); ?>