<?php
/**
 * @package WordPress
 * @subpackage atelier
 */

get_header(); ?>

<div id="content" class="nine columns float_right">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			
		<article id="post-<?php the_ID(); ?>">
			
            <h2><?php the_title(); ?></h2>
			
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            
		</article>
	
        <div class="pagination">
            <span class="align_right float_left"><?php previous_post_link( '%link', '' . _x( '&larr;', 'Previous post link', 'twentyten' ) . ' Previous' ); ?></span>
            <span class="align_left float_right"><?php next_post_link( '%link', 'Next ' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '' ); ?></span>
        </div>
        
	<?php endwhile; // end of the loop. ?>
        
</div>
    
<?php get_sidebar(); ?>

<?php get_footer(); ?>