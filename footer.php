<?php
/**
 * @package WordPress
 * @subpackage atelier
 */
?>

</div>

<footer class="row no_print">

<div class="eight columns">
     <p class="hide-on-phones">
     	<a href="/website-terms-of-use/" title="Website Terms Of Use">Website Terms of Use</a> |
        <a href="/accessibility/" title="Accessibility">Accessibility</a> |
        <a href="/privacy-policy/" title="Privacy">Privacy Policy &amp; Cookies</a> |
        <a href="/sitemap/" title="Sitemap">Sitemap</a>
     </p>

     <p>
     	&copy; <?php echo date("Y") ?> Company Name |  All Rights Reserved  |  Site by Atelier Studios  |  <a href="http://www.atelier-studios.com/" target="_blank">Digital Marketing Agency</a>
     </p>
</div>
<div class="four columns">
    <p class="align_right">
    	<a href="#top">Back To Top</a> | <a class="print" href="#print" title="Print This Page">Print This Page</a>
    </p>

	<p class="social_icons">
    	<a href="https://www.linkedin.com/" class="linkedin_icon" title="Visit our Linkedin page" target="_blank">Visit our Linkedin page</a>
     	<a href="http://twitter.com/" class="twitter_icon" title="Follow us on Twitter" target="_blank">Follow us on Twitter</a>
     	<a href="http://www.facebook.com/" class="facebook_icon" title="Add Us On Facebook" target="_blank">Add Us On Facebook</a>
     	<a href="/category/news/feed/" class="rss_icon" title="Subscribe To Our RSS Feed" target="_blank">Subscribe To Our RSS Feed</a>
     </p>

</div>

</footer>


<!-- IE OLD BROWSER WARNING -->


<div class="old_browser_warning no_print">
    <div class="padding_left_75_right_75 padding_top_40_bottom_40 row">

        <p class="align_centre white matiz font40">Uh Oh!</p>
        <p class="font20 align_centre">You seem to be using an old version of Internet Explorer.</p>

        <p class="align_centre padding_left_40_right_40">This isn't the end of the world, you can still use our website but you won't be able to enjoy the all of the features. Don't just take our word for it, find out more <a class="white" href="//www.smashingmagazine.com/2012/07/10/dear-web-user-please-upgrade-your-browser/">here</a>.To get the most out of your visit to our website we suggest you update your browser... its free!</p>

        <div class="align_centre padding_none margin margin_top_20">
            <div class="row">
                <div class="columns three"></div>
                <div class="columns three">
                    <a class="button medium green_but" href="//browsehappy.com/" target="_blank">See The Browsers</a>
                </div>
                <div class="columns three">
                    <a class="button medium red_but hide_warning" href="#">Ok Hide This Message</a>
                </div>
                <div class="columns three"></div>

            </div>
        </div>
    </div>
</div>  



</div>

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
</body>
</html>
