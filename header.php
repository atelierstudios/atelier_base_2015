<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" dir="ltr" lang="en-GB">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" dir="ltr" lang="en-GB">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" dir="ltr" lang="en-GB">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html dir="ltr" lang="en-GB">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="stylesheet" href="/wp-content/themes/atelier_base_2015/style.css" />
<link rel="Shortcut Icon" href="/wp-content/themes/atelier_base_2015/furniture/images/sitewide/fav-icon.png" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php
	wp_head();
?>


<?php if (
is_front_page()
) { ?>
	<script src="/wp-content/themes/atelier_base_2015/furniture/js/jquery.cycle.all.and.easing.js"></script>
	<script type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function($){
	$(document).ready(function() {

// =============== HOMEPAGE SLIDER FUNCTIONS - jQuery Cycle Plugin & Easing Plugin  ===============

		$(function() {
		   	$('div.slide_container').cycle({
			fx: 'scrollHorz',
			speed:  1500,
			easing:  'easeInOutExpo',
			timeout: 0,
			cleartypeNoBg: true,
			prev: 'a.previous',
			next: 'a.next',
			pager:  'ul.slider_nav',
        	pagerAnchorBuilder: pagerFactory
    		});

			function pagerFactory(idx, slide) {
				var s = idx > 2 ? '' : '';
				return '<li'+s+'><a href="#">'+(idx+1)+'</a></li>';
			};

		});

	});
	});
    </script>

<?php } ?>

<script src="/wp-content/themes/atelier_base_2015/furniture/js/jquery.cookiecuttr-and-jquery-cookie-plugin.js"></script>

<?php if (
is_page( 29 )
) { ?>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBH7t8fpLKv9xnV1Fl1i3YJMpyRHQJSs8k&sensor=false"></script>
    <script src="/wp-content/themes/atelier_base_2015/furniture/js/gmaps.js"></script>
	<script type="text/javascript">
    var map;
    $(document).ready(function(){

    <!-- http://hpneo.github.io/gmaps/documentation.html -->

      map = new GMaps({
        div: '#map',
        lat: 50.911027,
        lng: -1.405031
      });

      map.addMarker({
        lat: 50.911027,
        lng: -1.405031,
        icon: '/wp-content/themes/atelier_base_2015/furniture/images/sitewide/map-icon.png',
        title: 'Marker with InfoWindow',
        infoWindow: {
          content: '<p>HTML Content</p>'
        }
      });
    });
    </script>

<?php } ?>

<script src="/wp-content/themes/atelier_base_2015/furniture/js/functions.js"></script>


<!--[if lt IE 9]>
    <script src="/wp-content/themes/atelier_base_2015/furniture/js/selectivizr-min-andhtml-shim.js"></script>
	<link rel="stylesheet" media="print" href="/wp-content/themes/atelier_base_2015/furniture/css/ie-print-styles.css" />
<![endif]-->

<!-- NEW GOOGLE ANALYTICS CODE -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48530531-1', 'atelierhq.com');
  ga('send', 'pageview');

</script>



</head>
<body <?php body_class(); ?>>

<div id="top" class="row page_container_ie6 phone_container">

		<header class="no_print">

			<a class="logo" href="/" title="Back To Homepage">Client Name Goes Here</a>

			<a class="skip-link" href="#content">Skip to content</a>

			<nav>

			    <ul class="main_nav">
				<li><a href="/">Home</a></li>
				<li class="dropdown">
				    <a href="/about-us/">About Us</a>
				    <ul class="sub_menu">
				      <?php
				       $clean_page_list = wp_list_pages('title_li=&child_of=11&echo=0&sort_column=menu_order');
				       $clean_page_list = preg_replace('/title=\"(.*?)\"/','',$clean_page_list);
				       echo $clean_page_list;
				       ?>
				    </ul>
				</li>
				<li><a href="/category/news/">News</a></li>
				<li><a href="/contact-us/">Contact Us</a></li>
			 </ul>

			</nav>

		</header>

    <div class="row content_container">
