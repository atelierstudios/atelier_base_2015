<?php
/**
 * @package WordPress
 * @subpackage atelier
 */

get_header(); ?>


<div id="content" class="nine columns float_right">

		<?php while ( have_posts() ) : the_post(); ?>
        
        <article class="news_article_container row">
                        
            <div class="two columns phone-one">
                <p class="news_month"><?php the_time('M y') ?></p>
                <p class="news_day"><?php the_time('j') ?></p>
            </div>
                                            
            <div class="ten columns phone-three">
                <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                <p><? echo bm_better_excerpt(400, '... <a href="'.get_permalink().'">Read More &raquo;</a>'); ?></p>
            </div>
        
        </article>
        
        
        <?php endwhile; // End the loop. Whew. ?>
        
        <?php /* Display navigation to next/previous pages when applicable */ ?>
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                        
            <div class="pagination">
                <span class="align_left float_left"><?php next_posts_link( __( '&larr; Older Articles', 'twentyten' ) ); ?></span>
                <span class="align_right float_right"><?php previous_posts_link( __( 'Newer Articles &rarr;', 'twentyten' ) ); ?></span>
            </div>
                    
        <?php endif; ?>

</div>


<?php get_sidebar(); ?>

<?php get_footer(); ?>